#!/usr/bin/make -f
# -*- makefile -*-

#export DH_VERBOSE=1

include /usr/share/dpkg/pkg-info.mk
export DEB_VERSION_UPSTREAM
export POETRY_VERSION=$(shell poetry version | cut -f 2 -d " ")

export PYBUILD_NAME=sphinx_prompt

%:
	dh $@ --buildsystem=pybuild

override_dh_clean:
	rm -rf sphinx-prompt
	dh_clean

override_dh_auto_build:
	# Ensure the upstream version is set correctly in pyproject.toml file
	# before any build is running.
	@if [ "$$POETRY_VERSION" != "$$DEB_VERSION_UPSTREAM" ]; then \
	    echo "" ;\
	    echo "Error: Version string '$$POETRY_VERSION' in 'pyproject.toml'" ;\
	    echo "doesn't match upstream version '$$DEB_VERSION_UPSTREAM'!" ;\
	    echo "Version correctly adjusted through the patch queue?" ;\
	    echo "" ;\
	    exit 99 ;\
	fi
	cp -a sphinx_prompt sphinx-prompt
	dh_auto_build

override_dh_auto_test:
	dh_auto_test -- --system=custom --test-args="PYTHONPATH={build_dir} {interpreter} -m pytest tests"
